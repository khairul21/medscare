package com.example.marzuki92.medscare;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.marzuki92.medscare.adapter.MedicineAdapter;
import com.example.marzuki92.medscare.callback.RequestInterface;
import com.example.marzuki92.medscare.helper.Constants;
import com.example.marzuki92.medscare.model.Medicine;
import com.example.marzuki92.medscare.model.Patient;
import com.example.marzuki92.medscare.model.ServerRequest;
import com.example.marzuki92.medscare.model.ServerResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Marzuki92 on 11/5/2016.
 */
public class MedicineActivity extends Fragment {

    private RecyclerView recyclerView;
    private ArrayList<Medicine> medData;
    private MedicineAdapter medAdapter;
    private SharedPreferences pref;
    //private AppCompatButton btn_logout;
    private ProgressBar progress;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private String username;
    //private String json;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        pref = getActivity().getPreferences(0);

        View view = inflater.inflate(R.layout.activity_medicine, container, false);
        setHasOptionsMenu(true);
        initViews(view);

        return view;
    }

    private void initViews(View view) {

        //Bundle args = getArguments();
        //btn_logout = (AppCompatButton)view.findViewById(R.id.btn_logout);
        //btn_logout.setOnClickListener(this);
        progress = (ProgressBar)view.findViewById(R.id.progress);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.activity_main_swipe_refresh_layout);
        recyclerView = (RecyclerView) view.findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        username = pref.getString(Constants.USERNAME,"");
        loadJSON(username);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadJSON(username);
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                }, 2500);
            }
        });

        //Gson gson = new Gson();
        //json = pref.getString(Constants.REFERENCE.MEDICINE, null);
        //Type type = new TypeToken<ArrayList<Medicine>>() {}.getType();
        //medData = gson.fromJson(json, type);

        //medAdapter = new MedicineAdapter(getActivity(), medData);
        //recyclerView.setAdapter(medAdapter);

        //if(args != null) {
        //  medData = (ArrayList<Medicine>) args.getSerializable(Constants.REFERENCE.MEDICINE);
        //  medAdapter = new MedicineAdapter(getActivity(), medData);
        //  recyclerView.setAdapter(medAdapter);
        //}
    }

    private void loadJSON(String username) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RequestInterface requestInterface = retrofit.create(RequestInterface.class);

        Patient patient = new Patient();
        patient.setUsername(username);
        ServerRequest request = new ServerRequest();
        request.setOperation(Constants.MEDSDISPLAY_OPERATION);
        request.setPatient(patient);

        Call<ServerResponse> response = requestInterface.operation(request);
        response.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {

                ServerResponse resp = response.body();
                medData = new ArrayList<>(Arrays.asList(resp.getMedicine()));
                medAdapter = new MedicineAdapter(getActivity(), medData);
                recyclerView.setAdapter(medAdapter);
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {

                progress.setVisibility(View.INVISIBLE);
                Log.d(Constants.TAG,"failed");
                Snackbar.make(getView(), t.getLocalizedMessage(), Snackbar.LENGTH_LONG).show();
            }
        });
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.action_setting:
                Toast.makeText(getActivity(), "You select setting option", Toast.LENGTH_LONG).show();
                return true;

            case R.id.action_logout:
                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean(Constants.IS_LOGGED_IN,false);
                editor.putString(Constants.USERNAME,"");
                editor.putString(Constants.NAME,"");
                editor.apply();
                goToLogin();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void goToLogin(){
        Fragment login = new LoginFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame,login);
        ft.commit();
    }
}