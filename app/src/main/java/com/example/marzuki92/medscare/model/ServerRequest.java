package com.example.marzuki92.medscare.model;

/**
 * Created by Marzuki92 on 10/5/2016.
 */
public class ServerRequest {

    private String operation;
    private Patient patient;

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
