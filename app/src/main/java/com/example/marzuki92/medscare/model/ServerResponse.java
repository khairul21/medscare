package com.example.marzuki92.medscare.model;

/**
 * Created by Marzuki92 on 10/5/2016.
 */
public class ServerResponse {

    private String result;
    private String message;
    private Patient patient;
    private Medicine[] medicine;

    public String getResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }

    public Patient getPatient() {
        return patient;
    }

    public Medicine[] getMedicine() {
        return medicine;
    }
}
