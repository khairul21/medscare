package com.example.marzuki92.medscare.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.marzuki92.medscare.MedicineDisplayActivity;
import com.example.marzuki92.medscare.R;
import com.example.marzuki92.medscare.helper.Constants;
import com.example.marzuki92.medscare.model.Medicine;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Marzuki92 on 11/5/2016.
 */
public class MedicineAdapter extends RecyclerView.Adapter<MedicineAdapter.ViewHolder> {

    private ArrayList<Medicine> medicine;
    private Context context;

    public MedicineAdapter(Context context, ArrayList<Medicine> medicine){
        this.context = context;
        this.medicine = medicine;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.medicine_layout, parent, false);
        //ViewHolder viewHolder = new ViewHolder(view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Medicine currMedicine = medicine.get(position);

        holder.tv_medicine.setText(currMedicine.getName_med());
        Picasso.with(context).load("http://10.111.178.116/MedsCare/Public/" + currMedicine.getImage_path()).resize(120,60).into(holder.img_medicine);
    }

    @Override
    public int getItemCount() {
        return medicine.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tv_medicine;
        private ImageView img_medicine;
        private final Context context;
        private ArrayList<Medicine> mdc;

        public ViewHolder(View itemView) {
            super(itemView);

            context = itemView.getContext();
            this.mdc = medicine;

            tv_medicine = (TextView) itemView.findViewById(R.id.tv_medicine);
            img_medicine = (ImageView) itemView.findViewById(R.id.img_medicine);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            Medicine medPostn = this.mdc.get(position);
            Intent intent = new Intent(context, MedicineDisplayActivity.class);
            intent.putExtra(Constants.REFERENCE.MEDICINE, medPostn);
            context.startActivity(intent);
        }
    }
}
