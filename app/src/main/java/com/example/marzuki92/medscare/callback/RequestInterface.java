package com.example.marzuki92.medscare.callback;

import com.example.marzuki92.medscare.model.ServerRequest;
import com.example.marzuki92.medscare.model.ServerResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Marzuki92 on 10/5/2016.
 */
public interface RequestInterface {

    @POST("MedsCare/PhpScript/")
    Call<ServerResponse> operation(@Body ServerRequest request);
}
