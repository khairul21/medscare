package com.example.marzuki92.medscare;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.marzuki92.medscare.helper.Constants;
import com.example.marzuki92.medscare.model.Medicine;
import com.example.marzuki92.medscare.model.Patient;
import com.example.marzuki92.medscare.model.ServerResponse;

import java.util.ArrayList;

/**
 * Created by Marzuki92 on 10/5/2016.
 */
public class ProfileFragment extends Fragment implements View.OnClickListener {

    private TextView tv_name,tv_username, tv_age, tv_gender;
    private SharedPreferences pref;
    //private AppCompatButton btn_logout;
    //private AlertDialog dialog;
    //private ProgressBar progress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile,container,false);
        setHasOptionsMenu(true);
        initViews(view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        pref = getActivity().getPreferences(0);
        tv_username.setText("Welcome : " + pref.getString(Constants.USERNAME,""));
        tv_name.setText("Fullname : " + pref.getString(Constants.NAME,""));
        tv_age.setText("Age : " + pref.getString(Constants.AGE,""));
        tv_gender.setText("Gender : " + pref.getString(Constants.GENDER,""));

    }

    private void initViews(View view) {

        tv_name = (TextView)view.findViewById(R.id.tv_name);
        tv_username = (TextView)view.findViewById(R.id.tv_username);
        tv_gender = (TextView)view.findViewById(R.id.tv_gender);
        tv_age = (TextView)view.findViewById(R.id.tv_age);
        //btn_logout = (AppCompatButton)view.findViewById(R.id.btn_logout);
        //btn_logout.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){

            /*case R.id.btn_logout:
                logout();
                break;*/
        }
    }

    private void logout() {

    }

    private void goToLogin(){

        Fragment login = new LoginFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame,login);
        ft.commit();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.action_setting:
                Toast.makeText(getActivity(), "You select setting option", Toast.LENGTH_LONG).show();
                return true;

            case R.id.action_logout:
                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean(Constants.IS_LOGGED_IN,false);
                editor.putString(Constants.USERNAME,"");
                editor.putString(Constants.NAME,"");
                editor.apply();
                goToLogin();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
