package com.example.marzuki92.medscare;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.marzuki92.medscare.helper.Constants;
import com.example.marzuki92.medscare.model.Medicine;
import com.squareup.picasso.Picasso;

public class MedicineDisplayActivity extends AppCompatActivity {

    private ImageView imageView;
    private TextView tv_name,tv_description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicine_display);

        imageView = (ImageView) findViewById(R.id.imageView);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_description = (TextView) findViewById(R.id.tv_description);

        Intent intent = getIntent();

        Medicine medicine = (Medicine) intent.getSerializableExtra(Constants.REFERENCE.MEDICINE);

        Picasso.with(getApplicationContext()).load("http://10.111.178.116/MedsCare/Public/" + medicine.getImage_path()).into(imageView);
        tv_name.setText(medicine.getName_med());
        tv_description.setText(medicine.getDescription());

    }
}