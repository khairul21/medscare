package com.example.marzuki92.medscare;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.marzuki92.medscare.adapter.MedicineAdapter;
import com.example.marzuki92.medscare.callback.RequestInterface;
import com.example.marzuki92.medscare.helper.Constants;
import com.example.marzuki92.medscare.model.Medicine;
import com.example.marzuki92.medscare.model.Patient;
import com.example.marzuki92.medscare.model.ServerRequest;
import com.example.marzuki92.medscare.model.ServerResponse;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Marzuki92 on 10/5/2016.
 */
public class LoginFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = LoginFragment.class.getSimpleName();
    private AppCompatButton btn_login;
    private EditText et_username,et_password;
    private ProgressBar progress;
    private SharedPreferences pref;
    private Patient patient;
    //private ArrayList<Medicine> medData;
    //private String json;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login,container,false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        
        pref = getActivity().getPreferences(0);

        btn_login = (AppCompatButton)view.findViewById(R.id.btn_login);
        et_username = (EditText)view.findViewById(R.id.et_username);
        et_password = (EditText)view.findViewById(R.id.et_password);

        progress = (ProgressBar)view.findViewById(R.id.progress);

        btn_login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.btn_login:
                String username = et_username.getText().toString();
                String password = et_password.getText().toString();

                if(!username.isEmpty() && !password.isEmpty()) {

                    progress.setVisibility(View.VISIBLE);
                    loginProcess(username,password);

                } else {

                    Snackbar.make(getView(), "Fields are empty !", Snackbar.LENGTH_LONG).show();
                }
                break;
        }
        
    }

    private void loginProcess(String username, String password) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RequestInterface requestInterface = retrofit.create(RequestInterface.class);

        patient = new Patient();
        patient.setUsername(username);
        patient.setPassword(password);
        ServerRequest request = new ServerRequest();
        request.setOperation(Constants.LOGIN_OPERATION);
        request.setPatient(patient);

        Call<ServerResponse> response = requestInterface.operation(request);
        response.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {

                ServerResponse resp = response.body();
                Snackbar.make(getView(), resp.getMessage(), Snackbar.LENGTH_LONG).show();

                if(resp.getResult().equals(Constants.SUCCESS)){
                    SharedPreferences.Editor editor = pref.edit();
                    //Gson gson = new Gson();

                    //medData = new ArrayList<>(Arrays.asList(resp.getMedicine()));
                    //json = gson.toJson(medData);
                    editor.putBoolean(Constants.IS_LOGGED_IN, true);
                    editor.putString(Constants.USERNAME, resp.getPatient().getUsername());
                    editor.putString(Constants.NAME, resp.getPatient().getName());
                    editor.putString(Constants.AGE,resp.getPatient().getAge());
                    editor.putString(Constants.GENDER,resp.getPatient().getGender());
                    //editor.putString(Constants.REFERENCE.MEDICINE, json);
                    editor.apply();

                    goToProfile();

                }
                progress.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {

                progress.setVisibility(View.INVISIBLE);
                Log.d(Constants.TAG,"failed");
                Snackbar.make(getView(), t.getLocalizedMessage(), Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void goToProfile(){

        /*Fragment medDisp = MedicineActivity.newInstance(med);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame, medDisp);
        ft.commit();*/
        //Bundle bundle = new Bundle();
        //bundle.putSerializable(Constants.REFERENCE.MEDICINE, medData);
        Fragment profile = new MedicineActivity();
        //profile.setArguments(bundle);

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_frame, profile, TAG)
                .addToBackStack(TAG).commit();

        //FragmentTransaction ft = getFragmentManager().beginTransaction();
        //ft.replace(R.id.fragment_frame,profile);
        //ft.commit();
    }
}
