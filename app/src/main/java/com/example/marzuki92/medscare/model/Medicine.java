package com.example.marzuki92.medscare.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by Marzuki92 on 11/5/2016.
 */
public class Medicine implements Serializable {

    @Expose
    private String name_med;

    @Expose
    private String description;

    @Expose
    private String image_path;

    public String getName_med() {
        return name_med;
    }

    public String getDescription() {
        return description;
    }

    public String getImage_path() {
        return image_path;
    }
}