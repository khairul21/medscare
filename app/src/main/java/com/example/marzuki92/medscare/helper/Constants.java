package com.example.marzuki92.medscare.helper;

/**
 * Created by Marzuki92 on 10/5/2016.
 */
public class Constants {

    public static final String BASE_URL = "http://10.111.178.116/";
    //public static final String REGISTER_OPERATION = "register";
    public static final String LOGIN_OPERATION = "login";
    public static final String MEDSDISPLAY_OPERATION = "display";
    //public static final String CHANGE_PASSWORD_OPERATION = "chgPass";

    public static final String SUCCESS = "success";
    public static final String FAILURE = "failure";
    public static final String IS_LOGGED_IN = "isLoggedIn";

    public static final String NAME = "name";
    public static final String USERNAME = "username";
    public static final String AGE = "age";
    public static final String GENDER = "gender";
    //public static final String UNIQUE_ID = "unique_id";

    //public static final String RESET_PASSWORD_INITIATE = "resPassReq";
    //public static final String RESET_PASSWORD_FINISH = "resPass";

    public static final String TAG = "MK Innova";

    public static final class REFERENCE {

        public static final String MEDICINE = Config.PACKAGE_NAME + "medicine";
    }

    public static final class Config {

        public static final String PACKAGE_NAME = "com.example.marzuki92.medscare.";
    }

}
